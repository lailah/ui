# Lailah UI

# Thanks

## Nightscout
http://www.nightscout.info/

For showing me what people can do when a community builds together.

## dj-diabetes
https://github.com/push-things/dj-diabetes

For showing me that if you don't like something or something is lacking, build it yourself.

## Family

* For wanting to understand.
* For caring.
* For being there without question.

For keeping things simple when the world tries so hard to make it complex because everyone deserves a normal life.
Just because you have diabetes or autism doesn't mean you're special.  We will keep you in check when you're being a dick.  We will always have your back.

## Extended Family

* All the teachers.
* All the support workers.
* All the endocrinologists.
* All the co-workers.

Without all of you it's very easy to feel helpless and alone.


## Noah

For being an absolute rock star with being thrown this curveball while showing me that you don't give up.  You keep your smile and you change your definition of normal.

# Why Lailah?

Like many other people who invest their time and effort into fighting Diabetes I have a son that was diagnosed as Type One.
Beyond that, I am also blessed with the challenges of that same child being on the spectrum both for ASD and ADHD.

As with everything in life, when you are faced with challenges you have two choices.

1. Give Up
2. Power Through

This is me powering through.

# How Is Lailah Different?

I think the reasons for differentiation can be grouped into a few core areas.

## Managing Diabetes And The Spectrum

Just like Autism and ADHD, Diabetes does not go away when you get older.
Unfortunately it's also true that as you grow up those safety nets begin to disappear.
The world is not designed for you but that doesn't make you less important.

1. Lailah Grows With You.
    * Baselines Change
    * Holidays Happen
    * Life Throws Curve Balls
2. Independence Is Key.
    * Prompting Is Prioritized
    * Schedules Are Integrated
    * Limitations Are Respected
    * Helping Hands Are Welcomed

## Encouraging Community Involvement    

1. Simplified Code Base
    * Separate Backend/Frontend
    * Python/Django Backend
    * HTML/Vue JS Frontend
    * BYOD (Bring Your Own Datasource ie. MySQL, MariaDB, PostgreSQL, etc)
2. Integrations Are Key
    * Amazone Echo (tbd)
    * Google Home (tbd)    
3. Deploy It Anywhere
    * Python == Run It
    * Fully Self-Hosted Capabilities
4. Quick Deploy
    * One Touch It (tbd)

# Manual Install/Run

Run the following commands from the project folder.

`./manage.py makemigrations core food monitor`

`./manage.py migrate`

`./manage.py loaddata injectionsite.yaml insulin.yaml systemsetting.yaml calendar.yaml task.yaml bolus.yaml`

Run the following to launch the app.

`./manage.py runserver 0.0.0.0:8000`