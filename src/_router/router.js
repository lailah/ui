import Vue from "vue";
import VueRouter from 'vue-router'
import Home from "@/components/Home";
import Register from "@/components/Register";
import Login from "@/components/Login";
import Splash from "@/components/Splash";
import Monitor from "@/components/monitor/GlucoseMonitor";

Vue.use(VueRouter);

const routes = [
    { path: "/", name: "Home", component: Home },
    { path: "/register", name: "Register", component: Register },
    { path: "/login", name: "Login", component: Login },
    { path: "/splash", name: "Splash", component: Splash },
    { path: "/monitor", name: "Monitor", component: Monitor }
]

export const router = new VueRouter({
    mode: 'history',
    routes: routes,
    meta: { hideNavigation: true }
});

router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const publicPages = ['/', '/login', '/register'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('user');

    if (authRequired && !loggedIn) {
        return next('/');
    }
    next();
})